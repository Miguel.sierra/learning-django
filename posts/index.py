from django.http import HttpResponse
from django.http import JsonResponse
from django.shortcuts import render

def number(num):
    return num*2

def home(request):
    num = number(2)
    return HttpResponse('Hello world')

def user(request, username):
    return HttpResponse('Hello {}'.format(username))

def example(request):
    return render(request,'index.html',{'name': 'Miguel'})

def userJson(request,username):
    
    response = {
        'data': username,
        'status': 'ok'
    }
    return JsonResponse(response)
