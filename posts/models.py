from django.db import models

# Create your models here.

class posts(models.Model):
    #Post Model
    title = models.TextField(max_length=100,blank=False)
    publication = models.TextField(max_length=500)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    